![CaesarIA](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2013/Nov/12/caesaria-logo-3368332030-11_avatar.png)

![BuildStatus](https://drone.io/bitbucket.org/dalerank/caesaria/status.png)

![IndieDb Rating](http://button.indiedb.com/popularity/medium/games/27823.png)

CaesarIA is remake of Caesar III in the big and seamless world of Ancient Rome. You can build the small village with some of gardens and a workshop where you want.
You also can perform tasks of an emperor or build the whole city and broke attacks of barbarians, Egyptians or carfagens which want to steal your goods and to kill your inhabitants!
You also can pass the company from original game (when it will be restored)
The good lock with high walls will help to cope with any invasions. 

The fighting system of game continues and develops the principles from Caesar III. Legions of soldiers can be sent to any point of the card, but don't submit to direct instructions of the player, and work independently.
Depending on a situation soldiers can recede or be reformed for the best interaction.

The agriculture, extraction and production of goods will demand adjustment of production chains and if the necessary goods can't be got in the city, your trade partner can always sell it to you using
land or maritime trade routes.

Building CaesarIA
-------------------
See INSTALL_LINUX or INSTALL_WINDOWS depending on which operating system you are running.

#### External dependencies
CaesarIA is build using following excellent libraries:

  * SDL - http://www.libsdl.org/
  * SDL_mixer - http://www.libsdl.org/projects/SDL_mixer/
  * SDL_ttf - http://www.libsdl.org/projects/SDL_ttf/
    
